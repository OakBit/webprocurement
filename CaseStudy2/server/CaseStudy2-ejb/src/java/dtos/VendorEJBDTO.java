package dtos;

/**
 * EmployeeDTO - Container class that serializes employee information traveling
 * to and From EmployeeModel class
 */
import java.io.Serializable;

public class VendorEJBDTO implements Serializable {

    public VendorEJBDTO() {
    }
    private int id;
    private String address1;
    private String city;
    private String province;
    private String postalcode;
    private String phone;
    private String type;
    private String name;
    private String email;
    
    public String getProvince() {
        return province;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public String getPhone() {
        return phone;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int inValue) {
        this.id = inValue;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String inValue) {
        this.address1 = inValue;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String inValue) {
        this.city = inValue;
    }

}
