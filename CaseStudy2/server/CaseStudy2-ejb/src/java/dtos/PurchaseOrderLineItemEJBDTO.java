package dtos;

import java.io.Serializable;
import java.math.BigDecimal;

/*
* PurchaseOrderLineItemDTO.java
*
* Created: 9-10-2017 3:03 PM
* Purpose: Simple container class to house a single report item
*
* Revisions: @version 1.0, 09/10/17:
 */
public class PurchaseOrderLineItemEJBDTO implements Serializable {

    private int id;
    private int poid;
    private String productid;
    private int qty;
    private BigDecimal price;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPoid() {
        return poid;
    }

    public void setPoid(int poid) {
        this.poid = poid;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int quantity) {
        this.qty = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
