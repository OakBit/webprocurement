package dtos;

/**
 * ExpenseEJBDTO - Container class that serializes expense information traveling
 * to and From ProductEntity/ProductResource classes
 */
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Blob;

public class ProductEJBDTO implements Serializable {

    public ProductEJBDTO() {
    }
    private String id;
    private int vendorid;
    private String name;
    private BigDecimal costprice;
    private BigDecimal msrp;
    private int rop;
    private int eoq;
    private int qoh;
    private int qoo;
    private String qrcode;
    private String qrcodetxt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCostprice() {
        return costprice;
    }

    public void setCostprice(BigDecimal costprice) {
        this.costprice = costprice;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public int getRop() {
        return rop;
    }

    public void setRop(int rop) {
        this.rop = rop;
    }

    public int getEoq() {
        return eoq;
    }

    public void setEoq(int eoq) {
        this.eoq = eoq;
    }

    public int getQoh() {
        return qoh;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    public int getQoo() {
        return qoo;
    }

    public void setQoo(int qoo) {
        this.qoo = qoo;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String inValue) {
        qrcode = inValue;
    }

    public String getQrcodetxt() {
        return qrcodetxt;
    }

    public void setQrcodetxt(String qrcodetxt) {
        this.qrcodetxt = qrcodetxt;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String inValue) {
        this.id = inValue;
    }

    public int getVendorid() {
        return this.vendorid;
    }

    public void setVendorid(int inValue) {
        this.vendorid = inValue;
    }
}
