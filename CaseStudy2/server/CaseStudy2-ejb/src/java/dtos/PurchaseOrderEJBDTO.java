package dtos;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
/*
* ReportDTO.java
*
* Created: 9-10-2017 3:03 PM
* Purpose: Simple container class to house a single expense report
*
* Revisions: @version 1.0, 09/10/17
*/
public class PurchaseOrderEJBDTO implements Serializable {
private int id;
private int vendorid;
private BigDecimal amount;
private String podate;
private ArrayList<PurchaseOrderLineItemEJBDTO> items;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVendorid() {
        return vendorid;
    }

    public void setVendorid(int vendorid) {
        this.vendorid = vendorid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPodate() {
        return podate;
    }

    public void setPodate(String podate) {
        this.podate = podate;
    }

    public ArrayList<PurchaseOrderLineItemEJBDTO> getItems() {
        return items;
    }

    public void setItems(ArrayList<PurchaseOrderLineItemEJBDTO> items) {
        this.items = items;
    }
}