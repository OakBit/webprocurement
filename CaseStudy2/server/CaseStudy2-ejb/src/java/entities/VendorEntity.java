/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author downtime
 */
@Entity
@Table(name = "VENDORS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VendorEntity.findAll", query = "SELECT v FROM VendorEntity v")
    , @NamedQuery(name = "VendorEntity.findById", query = "SELECT v FROM VendorEntity v WHERE v.id = :id")
    , @NamedQuery(name = "VendorEntity.findByAddress1", query = "SELECT v FROM VendorEntity v WHERE v.address1 = :address1")
    , @NamedQuery(name = "VendorEntity.findByCity", query = "SELECT v FROM VendorEntity v WHERE v.city = :city")
    , @NamedQuery(name = "VendorEntity.findByProvince", query = "SELECT v FROM VendorEntity v WHERE v.province = :province")
    , @NamedQuery(name = "VendorEntity.findByPostalcode", query = "SELECT v FROM VendorEntity v WHERE v.postalcode = :postalcode")
    , @NamedQuery(name = "VendorEntity.findByPhone", query = "SELECT v FROM VendorEntity v WHERE v.phone = :phone")
    , @NamedQuery(name = "VendorEntity.findByType", query = "SELECT v FROM VendorEntity v WHERE v.type = :type")
    , @NamedQuery(name = "VendorEntity.findByName", query = "SELECT v FROM VendorEntity v WHERE v.name = :name")
    , @NamedQuery(name = "VendorEntity.findByEmail", query = "SELECT v FROM VendorEntity v WHERE v.email = :email")})
public class VendorEntity implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorid")
    private Collection<PurchaseorderEntity> purchaseorderEntityCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorid")
    private Collection<ProductEntity> productEntityCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ADDRESS1")
    private String address1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CITY")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "PROVINCE")
    private String province;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "POSTALCODE")
    private String postalcode;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "PHONE")
    private String phone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "TYPE")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "NAME")
    private String name;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "EMAIL")
    private String email;

    public VendorEntity() {
    }

    public VendorEntity(Integer id) {
        this.id = id;
    }

    public VendorEntity(Integer id, String address1, String city, String province, String postalcode, String phone, String type, String name, String email) {
        this.id = id;
        this.address1 = address1;
        this.city = city;
        this.province = province;
        this.postalcode = postalcode;
        this.phone = phone;
        this.type = type;
        this.name = name;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendorEntity)) {
            return false;
        }
        VendorEntity other = (VendorEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.VendorEntity[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<ProductEntity> getProductEntityCollection() {
        return productEntityCollection;
    }

    public void setProductEntityCollection(Collection<ProductEntity> productEntityCollection) {
        this.productEntityCollection = productEntityCollection;
    }

    @XmlTransient
    public Collection<PurchaseorderEntity> getPurchaseorderEntityCollection() {
        return purchaseorderEntityCollection;
    }

    public void setPurchaseorderEntityCollection(Collection<PurchaseorderEntity> purchaseorderEntityCollection) {
        this.purchaseorderEntityCollection = purchaseorderEntityCollection;
    }
    
}
