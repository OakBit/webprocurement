/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author downtime
 */
@Entity
@Table(name = "PRODUCTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductEntity.findAll", query = "SELECT p FROM ProductEntity p")
    , @NamedQuery(name = "ProductEntity.findById", query = "SELECT p FROM ProductEntity p WHERE p.id = :id")
    , @NamedQuery(name = "ProductEntity.findByName", query = "SELECT p FROM ProductEntity p WHERE p.name = :name")
    , @NamedQuery(name = "ProductEntity.findByCostprice", query = "SELECT p FROM ProductEntity p WHERE p.costprice = :costprice")
    , @NamedQuery(name = "ProductEntity.findByMsrp", query = "SELECT p FROM ProductEntity p WHERE p.msrp = :msrp")
    , @NamedQuery(name = "ProductEntity.findByRop", query = "SELECT p FROM ProductEntity p WHERE p.rop = :rop")
    , @NamedQuery(name = "ProductEntity.findByEoq", query = "SELECT p FROM ProductEntity p WHERE p.eoq = :eoq")
    , @NamedQuery(name = "ProductEntity.findByQoh", query = "SELECT p FROM ProductEntity p WHERE p.qoh = :qoh")
    , @NamedQuery(name = "ProductEntity.findByQoo", query = "SELECT p FROM ProductEntity p WHERE p.qoo = :qoo")
    , @NamedQuery(name = "ProductEntity.findByQrcodetxt", query = "SELECT p FROM ProductEntity p WHERE p.qrcodetxt = :qrcodetxt")})
public class ProductEntity implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productid")
    private Collection<PurchaseorderlineitemEntity> purchaseorderlineitemEntityCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "ID")
    private String id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NAME")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "COSTPRICE")
    private BigDecimal costprice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MSRP")
    private BigDecimal msrp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ROP")
    private int rop;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EOQ")
    private int eoq;
    @Basic(optional = false)
    @NotNull
    @Column(name = "QOH")
    private int qoh;
    @Basic(optional = false)
    @NotNull
    @Column(name = "QOO")
    private int qoo;
    @Lob
    @Column(name = "QRCODE")
    private Serializable qrcode;
    @Size(max = 50)
    @Column(name = "QRCODETXT")
    private String qrcodetxt;
    @JoinColumn(name = "VENDORID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private VendorEntity vendorid;

    public ProductEntity() {
    }

    public ProductEntity(String id) {
        this.id = id;
    }

    public ProductEntity(String id, String name, BigDecimal costprice, BigDecimal msrp, int rop, int eoq, int qoh, int qoo) {
        this.id = id;
        this.name = name;
        this.costprice = costprice;
        this.msrp = msrp;
        this.rop = rop;
        this.eoq = eoq;
        this.qoh = qoh;
        this.qoo = qoo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCostprice() {
        return costprice;
    }

    public void setCostprice(BigDecimal costprice) {
        this.costprice = costprice;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public int getRop() {
        return rop;
    }

    public void setRop(int rop) {
        this.rop = rop;
    }

    public int getEoq() {
        return eoq;
    }

    public void setEoq(int eoq) {
        this.eoq = eoq;
    }

    public int getQoh() {
        return qoh;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    public int getQoo() {
        return qoo;
    }

    public void setQoo(int qoo) {
        this.qoo = qoo;
    }

    public Serializable getQrcode() {
        return qrcode;
    }

    public void setQrcode(Serializable qrcode) {
        this.qrcode = qrcode;
    }

    public String getQrcodetxt() {
        return qrcodetxt;
    }

    public void setQrcodetxt(String qrcodetxt) {
        this.qrcodetxt = qrcodetxt;
    }

    public VendorEntity getVendorid() {
        return vendorid;
    }

    public void setVendorid(VendorEntity vendorid) {
        this.vendorid = vendorid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductEntity)) {
            return false;
        }
        ProductEntity other = (ProductEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ProductEntity[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<PurchaseorderlineitemEntity> getPurchaseorderlineitemEntityCollection() {
        return purchaseorderlineitemEntityCollection;
    }

    public void setPurchaseorderlineitemEntityCollection(Collection<PurchaseorderlineitemEntity> purchaseorderlineitemEntityCollection) {
        this.purchaseorderlineitemEntityCollection = purchaseorderlineitemEntityCollection;
    }
    
}
