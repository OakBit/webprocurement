/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author downtime
 */
@Entity
@Table(name = "PURCHASEORDERLINEITEMS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PurchaseorderlineitemEntity.findAll", query = "SELECT p FROM PurchaseorderlineitemEntity p")
    , @NamedQuery(name = "PurchaseorderlineitemEntity.findById", query = "SELECT p FROM PurchaseorderlineitemEntity p WHERE p.id = :id")
    , @NamedQuery(name = "PurchaseorderlineitemEntity.findByQty", query = "SELECT p FROM PurchaseorderlineitemEntity p WHERE p.qty = :qty")
    , @NamedQuery(name = "PurchaseorderlineitemEntity.findByPrice", query = "SELECT p FROM PurchaseorderlineitemEntity p WHERE p.price = :price")})
public class PurchaseorderlineitemEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "QTY")
    private int qty;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRICE")
    private BigDecimal price;
    @JoinColumn(name = "PRODUCTID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private ProductEntity productid;
    @JoinColumn(name = "POID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private PurchaseorderEntity poid;

    public PurchaseorderlineitemEntity() {
    }

    public PurchaseorderlineitemEntity(Integer id) {
        this.id = id;
    }

    public PurchaseorderlineitemEntity(Integer id, int qty, BigDecimal price) {
        this.id = id;
        this.qty = qty;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ProductEntity getProductid() {
        return productid;
    }

    public void setProductid(ProductEntity productid) {
        this.productid = productid;
    }

    public PurchaseorderEntity getPoid() {
        return poid;
    }

    public void setPoid(PurchaseorderEntity poid) {
        this.poid = poid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PurchaseorderlineitemEntity)) {
            return false;
        }
        PurchaseorderlineitemEntity other = (PurchaseorderlineitemEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PurchaseorderlineitemEntity[ id=" + id + " ]";
    }
    
}
