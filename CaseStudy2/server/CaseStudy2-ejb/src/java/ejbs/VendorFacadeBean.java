package ejbs;

import dtos.VendorEJBDTO;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import entities.VendorEntity;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolationException;

@Stateless
@LocalBean
public class VendorFacadeBean extends AbstractFacade<VendorEntity> {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    private final UtilityMethods util;

    public VendorFacadeBean() {
        super(VendorEntity.class);
        util = new UtilityMethods();
    }

    /**
     * @param dto vendor DTO of all new information
     * @return int representing the PK of the new vendor row.
     */
    public int add(VendorEJBDTO dto) {
        int retId = -1;
        try {
            VendorEntity ent = util.loadEntityFromDTO(dto, new VendorEntity());
            create(ent);
            em.flush();
            retId = ent.getId();
        } catch (ConstraintViolationException vex) {
            util.determineConstraintViolation(vex);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return retId;
    }

    /**
     * @param id int represent vendor PK
     * @return int representing the number of rows deleted.
     */
    public int delete(int id) {
        int rowsDeleted = -1;
        try {
            remove(find(id));
            em.flush();
            rowsDeleted = 1;
        } catch (Exception e) {
            System.out.println("problem deleting " + e.getMessage());
        }
        return rowsDeleted;
    }

    /**
     * @param dto vendor DTO of all updated information
     * @return int representing the number of rows updated.
     */
    public int update(VendorEJBDTO dto) {
        int vendUpdated = -1;
        try {
            edit(util.loadEntityFromDTO(dto, find(dto.getId())));
            em.flush();
            vendUpdated = 1;
        } catch (ConstraintViolationException vex) {
            util.determineConstraintViolation(vex);
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
        return vendUpdated;
    }

    /**
     * @return List of DTOs representing all vendors
     */
    public List<VendorEJBDTO> getAll() {
        List<VendorEJBDTO> vendorsDTO = new ArrayList();
        try {
// using lambda to map employee entity to dto
            findAll().stream().map((e) -> {
                return util.loadDTOFromEntity(new VendorEJBDTO(), e);
            }).forEach(vendorsDTO::add);
        } catch (Exception e) {
//Handle other errors
            System.out.println("other issue " + e.getMessage());
        }
        return vendorsDTO;
    }

    public VendorEJBDTO getOne(int id) {
        return util.loadDTOFromEntity(new VendorEJBDTO(), find(id));
    }
}
