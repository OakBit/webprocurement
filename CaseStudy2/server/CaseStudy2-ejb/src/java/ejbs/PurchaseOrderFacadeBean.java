package ejbs;

import dtos.PurchaseOrderEJBDTO;
import dtos.PurchaseOrderLineItemEJBDTO;
import entities.ProductEntity;
import entities.PurchaseorderEntity;
import entities.PurchaseorderlineitemEntity;
import entities.VendorEntity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

/**
 * @author downtime
 */
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Stateless
@LocalBean
public class PurchaseOrderFacadeBean extends AbstractFacade<PurchaseorderEntity> {

    private final UtilityMethods util;
    @Resource
    private EJBContext context;
    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PurchaseOrderFacadeBean() {
        super(PurchaseorderEntity.class);
        util = new UtilityMethods();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int add(PurchaseOrderEJBDTO dto) {
        PurchaseorderEntity order;
        VendorEntity vendor;
        int orderId = -1;
        try {
            vendor = em.find(VendorEntity.class, dto.getVendorid());
            order = new PurchaseorderEntity();
            order.setPodate(new Date());
            order.setVendorid(vendor);
            order.setAmount(dto.getAmount());
            create(order);
            em.flush();
            orderId = order.getId();
            
            Collection<PurchaseorderlineitemEntity> tmpLines = new ArrayList<>();
            dto.getItems().stream().map((item) -> {
                PurchaseorderlineitemEntity orderitem = new PurchaseorderlineitemEntity();
                ProductEntity product = em.find(ProductEntity.class, item.getProductid());
                orderitem.setProductid(product);
                orderitem.setPoid(order);
                orderitem.setPrice(item.getPrice());
                orderitem.setQty(item.getQty());
                
                product.setQoo(item.getQty() + product.getQoo());
                product.setQoh(product.getQoh() - item.getQty());
                em.merge(product); 
                
                return orderitem;
            }).forEachOrdered((orderitem) -> {
                tmpLines.add(orderitem);
            });
// add to entity
            order.setPurchaseorderlineitemEntityCollection(tmpLines);
            create(order);
        } catch (Exception e) {
            context.setRollbackOnly();
            orderId = -1; //in case something happened processing items
            System.out.println(e.getMessage());
        }
        return orderId;
    }

    /**
     * @param id representing the report we're interested in
     * @return all details for single report
     */
    public PurchaseOrderEJBDTO get(int id) {
        ArrayList<PurchaseOrderLineItemEJBDTO> items;
        PurchaseorderEntity order;
        PurchaseOrderEJBDTO dto = new PurchaseOrderEJBDTO();
        try {
            order = find(id);
            dto.setPodate(util.formatDate(order.getPodate()));
            dto.setId(order.getId());
            dto.setVendorid(order.getVendorid().getId());
            items = new ArrayList<>();
// line item loop
            order.getPurchaseorderlineitemEntityCollection().stream().map((item) -> {
                PurchaseOrderLineItemEJBDTO lineinfo = new PurchaseOrderLineItemEJBDTO();
                lineinfo.setPoid(id);
                lineinfo.setProductid(item.getProductid().getId());
                lineinfo.setPrice(item.getPrice());
                lineinfo.setQty(item.getQty());
                return lineinfo;
            }).forEachOrdered((lineinfo) -> {
                items.add(lineinfo);
            });
            dto.setItems(items);
            dto.setAmount(order.getAmount());
        } catch (Exception e) {
            System.out.println("Error getting line info from ExpenseReportFacade - ");
        }
        return dto;
    }
}
