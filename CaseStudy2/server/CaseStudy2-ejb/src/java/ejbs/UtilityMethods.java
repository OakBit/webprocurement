package ejbs;

import dtos.ProductEJBDTO;
import javax.persistence.EntityManager;
import entities.VendorEntity;
import dtos.VendorEJBDTO;
import entities.ProductEntity;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import org.hibernate.validator.internal.engine.path.PathImpl;

/**
 * @author Evan
 */
public class UtilityMethods {

    public VendorEntity loadEntityFromDTO(VendorEJBDTO dto, VendorEntity ent) {
        ent.setAddress1(dto.getAddress1());
        ent.setCity(dto.getCity());
        ent.setEmail(dto.getEmail());
        ent.setId(dto.getId());
        ent.setName(dto.getName());
        ent.setPhone(dto.getPhone());
        ent.setPostalcode(dto.getPostalcode());
        ent.setProvince(dto.getProvince());
        ent.setType(dto.getType());
        return ent;
    }

    public VendorEJBDTO loadDTOFromEntity(VendorEJBDTO dto, VendorEntity ent) {
        dto.setAddress1(ent.getAddress1());
        dto.setCity(ent.getCity());
        dto.setEmail(ent.getEmail());
        dto.setId(ent.getId());
        dto.setName(ent.getName());
        dto.setPhone(ent.getPhone());
        dto.setPostalcode(ent.getPostalcode());
        dto.setProvince(ent.getProvince());
        dto.setType(ent.getType());
        return dto;
    }

    public void determineConstraintViolation(ConstraintViolationException vex) {
        Set<ConstraintViolation<?>> set = vex.getConstraintViolations();
        set.forEach((next) -> {
            System.out.println(((PathImpl) next.getPropertyPath())
                    .getLeafNode().getName() + " " + next.getMessage());
        });
    }

    public ProductEntity loadEntityFromDTO(ProductEJBDTO dto, ProductEntity ent, EntityManager em) {
        ent.setCostprice(dto.getCostprice());
        ent.setId(dto.getId());
        ent.setEoq(dto.getEoq());
        ent.setMsrp(dto.getMsrp());
        ent.setCostprice(dto.getCostprice());
        ent.setName(dto.getName());
        ent.setQoh(dto.getQoh());
        ent.setQoo(dto.getQoo());
        ent.setQrcode(QRCode.from(dto.getQrcodetxt()).to(ImageType.PNG).stream().toByteArray());        ent.setQrcodetxt(dto.getQrcodetxt());
        ent.setRop(dto.getRop());
        VendorEntity vendent = em.find(VendorEntity.class, dto.getVendorid());
        ent.setVendorid(vendent);

        return ent;
    }

    public ProductEJBDTO loadDTOFromEntity(ProductEJBDTO dto, ProductEntity ent) {
        dto.setCostprice(dto.getCostprice());
        dto.setId(ent.getId());
        dto.setEoq(ent.getEoq());
        dto.setMsrp(ent.getMsrp());
        dto.setCostprice(ent.getCostprice());
        dto.setName(ent.getName());
        dto.setQoh(ent.getQoh());
        dto.setQoo(ent.getQoo());
        if (ent.getQrcode() != null) {
            dto.setQrcode(Base64.getEncoder().encodeToString((byte[]) ent.getQrcode()));
        }
        dto.setQrcodetxt(ent.getQrcodetxt());
        dto.setRop(ent.getRop());
        dto.setVendorid(ent.getVendorid().getId());

        return dto;
    }

    public String formatDate(Date date) {
        String formattedDate = "";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            formattedDate = df.format(date);
        } catch (Exception e) {
        }
        return formattedDate;
    }

    public Date formatDate(String date) {
        Date formattedDate = null;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            formattedDate = df.parse(date);
        } catch (Exception e) {
        }
        return formattedDate;
    }
}
