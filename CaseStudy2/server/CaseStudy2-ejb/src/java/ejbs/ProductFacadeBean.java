package ejbs;

import dtos.ProductEJBDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import entities.ProductEntity;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author downtime
 */
@Stateless
public class ProductFacadeBean extends AbstractFacade<ProductEntity> {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    private final UtilityMethods util;

    public ProductFacadeBean() {
        super(ProductEntity.class);
        util = new UtilityMethods();
    }

    /**
     * @param id String represent product PK
     * @return int representing the number of rows deleted.
     */
    public int delete(String id) {
        int rowsDeleted = -1;
        try {
            remove(find(id));
            rowsDeleted = 1;
        } catch (Exception e) {
            System.out.println("problem deleting " + e.getMessage());
        }
        return rowsDeleted;
    }

    /**
     * @param dto expense DTO of all new information
     * @return String representing the PK of the new product row.
     */
    public int add(ProductEJBDTO dto) {
        int num = -1;
        try {
            ProductEntity ent = util.loadEntityFromDTO(dto, new ProductEntity(), em);
            create(ent);
            em.flush();
            num = 1;
        } catch (ConstraintViolationException vex) {
            util.determineConstraintViolation(vex);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return num;
    }

    /**
     * @param dto expense DTO of all updated information
     * @return int representing the number of rows updated.
     */
    public int update(ProductEJBDTO dto) {
        int expUpdated = -1;
        try {
            edit(util.loadEntityFromDTO(dto, find(dto.getId()), em));
            em.flush();
            expUpdated = 1;
        } catch (ConstraintViolationException vex) {
            util.determineConstraintViolation(vex);
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
        return expUpdated;
    }

    /**
     * @return List of DTOs representing all products
     */
    public List<ProductEJBDTO> getAll() {
        List<ProductEJBDTO> productsDTO = new ArrayList();
        try {
// using lambda to map product entity to dto
            findAll().stream().map((e) -> {
                return util.loadDTOFromEntity(new ProductEJBDTO(), e);
            }).forEach(productsDTO::add);
        } catch (Exception e) {
//Handle other errors
            System.out.println("other issue " + e.getMessage());
        }
        return productsDTO;
    }

    /**
     * @param id String representing PK of product table
     * @return DTO representing a single product
     */
    public ProductEJBDTO getOne(String id) {
        return util.loadDTOFromEntity(new ProductEJBDTO(), find(id));
    }

}
